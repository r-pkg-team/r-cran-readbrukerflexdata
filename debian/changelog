r-cran-readbrukerflexdata (1.9.3-2) UNRELEASED; urgency=medium

  * Replace Filippo + Sebastian by myself as Uploader (Thank you Filippo
    and Sebastian for your previous work on this package)

 -- Andreas Tille <tille@debian.org>  Thu, 09 Jan 2025 07:39:35 +0100

r-cran-readbrukerflexdata (1.9.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Charles Plessy <plessy@debian.org>  Thu, 10 Oct 2024 08:56:54 +0900

r-cran-readbrukerflexdata (1.9.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Add autopkgtest

 -- Andreas Tille <tille@debian.org>  Wed, 31 Jan 2024 22:03:26 +0100

r-cran-readbrukerflexdata (1.9.1-1) unstable; urgency=medium

  * Team upload.
  * Disable reprotest
  * Standards-Version: 4.6.2 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Mon, 26 Jun 2023 19:34:37 +0200

r-cran-readbrukerflexdata (1.9.0-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Archive, Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.

  [ Andreas Tille ]
  * Standards-Version: 4.6.1 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 24 Jun 2022 11:13:21 +0200

r-cran-readbrukerflexdata (1.8.5-3) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Drop useless get-orig-source target (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Trim trailing whitespace.

 -- Dylan Aïssi <daissi@debian.org>  Sat, 16 May 2020 11:53:35 +0200

r-cran-readbrukerflexdata (1.8.5-2) unstable; urgency=medium

  * Team upload.
  * Rebuild for R 3.5 transition
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends
  * Secure URI in watch file

 -- Andreas Tille <tille@debian.org>  Thu, 31 May 2018 21:58:28 +0200

r-cran-readbrukerflexdata (1.8.5-1) unstable; urgency=low

  [ Sebastian Gibb ]
  * New upstream release.
  * debian/control:
    - Add Sebastian Gibb to Uploaders.

  [ Andreas Tille ]
  * Standards-Version: 4.1.0
  * Add debian/README.source to document binary data files

 -- Sebastian Gibb <sgibb.debian@gmail.com>  Fri, 29 Sep 2017 20:08:12 +0200

r-cran-readbrukerflexdata (1.8.3-1) unstable; urgency=medium

  * Team upload

  [ Sebastian Gibb ]
  * New upstream release.
  * debian/control:
    - Set Standards-Version to 3.9.8.
    - Use secure canonical URL in Homepage.
    - Use secure https URLs in Vcs-Git and Vcs-Browser.

  [ Andreas Tille ]
  * cme fix dpkg-control
  * Convert to dh-r
  * Architecture: all
  * debhelper 10
  * Update d/copyright
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Mon, 12 Dec 2016 15:23:25 +0100

r-cran-readbrukerflexdata (1.8.2-1) unstable; urgency=low

  * New upstream release.
  * debian/control.in: set Standards-Version to 3.9.6.
  * Replace own build script by a cdbs based one:
    - debian/control.in: removed.
    - debian/control: add cdbs to Build-Depends.
    - debian/rules: use cdbs based build script.
  * debian/copyright:
      - Change GPL-3 to GPL-3+.
      - Remove duplicated license text.

  * Upload by Filippo Rusconi <lopippo@debian.org>

 -- Filippo Rusconi <lopippo@debian.org>  Thu, 02 Jul 2015 16:41:23 +0200

r-cran-readbrukerflexdata (1.8-1) unstable; urgency=low

  [ The Debichem Group ]
  * New upstream release.
  * debian/control.in: set Standards-Version to 3.9.5.

  [ Filippo Rusconi ] <lopippo@debian.org>
  * Upload.

 -- Filippo Rusconi <lopippo@debian.org>  Tue, 30 Sep 2014 09:39:28 +0200

r-cran-readbrukerflexdata (1.7-1) unstable; urgency=low

  * New upstream release.
  * debian/control.in:
    - use canonical URI in Vcs-Git/Vcs-Browser.
    - set debhelper (>= 9)
  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Wed, 28 Aug 2013 13:42:19 +0200

r-cran-readbrukerflexdata (1.6.3-1) unstable; urgency=low

  * New upstream release. Upload by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Sun, 28 Apr 2013 21:12:31 +0200

r-cran-readbrukerflexdata (1.6.1-2) unstable; urgency=low

  * debian/control: update Standards-Version

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Thu, 28 Mar 2013 10:33:04 +0100

r-cran-readbrukerflexdata (1.6.1-1) unstable; urgency=low

  * New upstream release.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Thu, 28 Mar 2013 09:47:40 +0100

r-cran-readbrukerflexdata (1.6-1) unstable; urgency=low

  * New upstream release.

  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Thu, 13 Dec 2012 16:50:23 +0100

r-cran-readbrukerflexdata (1.5-1) unstable; urgency=low

  * Initial release by Sebastian Gibb <sgibb.debian@gmail.com>
    (Closes: #693738).

  * Uploaded by Filippo Rusconi <lopippo@debian.org>.

 -- The Debichem Group <debichem-devel@lists.alioth.debian.org>  Sun, 11 Nov 2012 10:44:00 +0100
